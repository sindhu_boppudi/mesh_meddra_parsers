import MEDDRA_Rest_API as t1
import MEDDRA_MeSH_Mapping as t2

meddra_terms = t1.print_properties()
meddra_map_terms = t2.print_mappings()
union_dict = meddra_map_terms

for term, val in meddra_terms.items():
    if term not in meddra_map_terms:
        union_dict[term] = val

print(union_dict)

f1 = open("Unique_MEDDRA_Terms_4.txt", "w+")

for key, val in union_dict.items():
    f1.write("Term : " + key)
    f1.write("\n")
    for i in val:
        for k, v in i.items():
            if k == "Term_Notation":
                f1.write("Term_Notation : " + v)
                f1.write("\n")
            if k == "Classified_as":
                f1.write("Classified_as : " + v)
                f1.write("\n")
            if k == "Classified_as_Notation":
                f1.write("Classified_as_Notation : " + v)
                f1.write("\n")
            if k == 'Classifies':
                f1.write("Classifies : " + v)
                f1.write("\n")
            if k == "Classifies_Notation":
                f1.write("Classifies_Notation : " + v)
                f1.write("\n")


f1.close()























# meddra_terms = []
#
# meddra_mapped_terms = []
#
#
# lines_text1 = [line.rstrip('\n') for line in open('MEDDRA_OUTPUT_8.txt')]
#
# meddra_output = {}
#
# for i in lines_text1:
#     if 'Term' in i:
#         x = i.split(" : ")
#         if x[0] == "Term":
#              k = x[1]
#     if 'Term_Notation' in i:
#         x = i.split(" : ")
#         if x[0] == "Term_Notation":
#              v = x[1]
#
#         meddra_output[k] = v
#
# lines_text2 = [line.rstrip('\n') for line in open('MeSH_MEDDRA_Mapping_8.txt')]
#
# meddra_mapped_output = {}
#
# for i in lines_text2:
#     if 'Term' in i:
#         x = i.split(" : ")
#         if x[0] == "Term":
#              k = x[1]
#     if 'Term_Notation' in i:
#         x = i.split(" : ")
#         if x[0] == "Term_Notation":
#              v = x[1]
#
#         meddra_mapped_output[k] = v
#
# final_terms = meddra_mapped_output
#
# for term, val in meddra_output.items():
#     if term not in meddra_mapped_output:
#         # print("{} : {}".format(term,val))
#         final_terms[term] = val
#
# f1 = open("Unique_MEDDRA_Terms_8.txt", "w+")
#
# for k, v in final_terms.items():
#     f1.write("Term : " + k)
#     f1.write("\n")
#     f1.write("Notation : " + v)
#     f1.write("\n")
#
# f1.close()