import urllib.response
import urllib.request
import urllib.parse
import json
import unicodedata
import re
import sys

REST_URL = "http://data.bioontology.org"
API_KEY = "6e00a808-51d9-4381-a593-787674286a98"

result_url = []
classes_terms = []
search_results = []
classes_dict = dict()
classified_as_dict = dict()
classifies_dict = dict()
result_dict = dict()

f1 = open("MESH_OUTPUT_4.txt", "w+")

def get_json(url):
    opener = urllib.request.build_opener()
    opener.addheaders = [('Authorization', 'apikey token=' + API_KEY)]
    url1 = url + "?include=all"
    return json.loads(opener.open(url1).read())


def print_annotations(annotations, get_class=True):
    for result in annotations:
        class_details = get_json(result["annotatedClass"]["links"]["self"]) if get_class else result["annotatedClass"]
        classes_terms.append(class_details)
        f1.write("MeSH Term : " + class_details["prefLabel"])
        f1.write("\n")
        f1.write("Notation : " + class_details["notation"])
        f1.write("\n")
        f1.write("Mapping Link : " + result["annotatedClass"]["links"]["mappings"])
        f1.write("\n")

def getontologyforfilter():

    return "MESH"

raw_text = """Cost Effectiveness of Pembrolizumab for Advanced Melanoma Treatment in Portugal.BACKGROUND: The aim of this study was to assess the cost-effectiveness of pembrolizumab in treating patients with ipilimumab-naïve advanced melanoma in Portugal. METHODS: A cost-effectiveness model was developed to analyze the costs and consequences of treatment with pembrolizumab compared to treatment with ipilimumab in patients with advanced melanoma not previously treated with ipilimumab. The model was parameterized by using data from a head-to-head phase III randomized clinical trial, KEYNOTE-006. Extrapolation of long-term outcomes was based on approaches previously applied, combining ipilimumab data and melanoma patients' registry data. The analysis was conducted from the perspective of the Portuguese National Health Service, and a lifetime horizon (40 years) was used. Portugal-specific disease management costs were estimated by convening a panel of six clinical experts to derive health state resource use and multiplying the results by national unit costs. To test for the robustness of the conclusions, we conducted deterministic and probabilistic sensitivity analyses. RESULTS: Pembrolizumab increases life expectancy in 1.57 undiscounted life-years (LYs) and is associated with an increase in costs versus that of ipilimumab. The estimated incremental cost-effectiveness ratio is 47,221 per quality-adjusted life-year (QALY) and 42,956 per LY. Deterministic sensitivity analysis showed that the results were robust to the change of most input values or assumptions and were sensitive to time on treatment scenarios. According to the probabilistic sensitivity analysis performed, pembrolizumab is associated with a cost per QALY gained inferior to 50,000 in 75% of the cases. CONCLUSIONS: Considering the usually accepted thresholds in oncology, pembrolizumab is a cost-effective alternative for treating patients with advanced melanoma in Portugal."""

text_to_annotate = unicodedata.normalize('NFKD', raw_text).encode('ascii', 'ignore')

# Annotate using the provided text
annotations = get_json(REST_URL + "/annotator?text=" + urllib.parse.quote(text_to_annotate) + "&ontologies=" + getontologyforfilter() + "&longest_only=false&exclude_numbers=false&whole_word_only=true&exclude_synonyms=false?include=all")

# Print out annotation details
print_annotations(annotations)

f1.close()

'''
def print_properties(properties, get_class=True):
    for result in classes_terms:
        f1.write("Class : " + result["prefLabel"])
        f1.write("\n")
        f1.write("Notation : " + result["notation"])
        f1.write("\n\n")
        for res1, val in result["properties"].items():
            if "classified_as" in res1:
                for val1 in val:
                    val2 = val1.rsplit('/', 1)[-1]
                    res3 = result["links"]["self"]
                    res4 = re.split('[a-f]+', res3, flags=re.IGNORECASE)
                    res5 = res3.replace(res4[-1], val2)
                    res6 = get_json(res5)
                    notation = res6["notation"]
                    f1.write("Classified as : " + res6["prefLabel"])
                    f1.write("\n")
                    f1.write("Notation : " + notation)
                    f1.write("\n")
                    # classified_as_dict[res6["prefLabel"]] = notation
                    for res7, val in res6["properties"].items():
                        if "classifies" in res7:
                            for val1 in val:
                                val2 = val1.rsplit('/', 1)[-1]
                                res7 = result["links"]["self"]
                                res8 = re.split('[a-f]+', res7, flags=re.IGNORECASE)
                                res9 = res7.replace(res8[-1], val2)
                                res10 = get_json(res9)
                                notation10 = res10["notation"]
                                f1.write("Classifies : " + res10["prefLabel"])
                                f1.write("\n")
                                f1.write("Notation : " + notation10)
                                f1.write("\n")
                            # classifies_dict[res10["prefLabel"]] = notation
                            # res_classifies.append(res6["prefLabel"])
            elif "classifies" in res1:
                for val1 in val:
                    val2 = val1.rsplit('/', 1)[-1]
                    res3 = result["links"]["self"]
                    res4 = re.split('[a-f]+', res3, flags=re.IGNORECASE)
                    res5 = res3.replace(res4[-1], val2)
                    res6 = get_json(res5)
                    notation = res6["notation"]
                    # classifies_dict[res6["prefLabel"]] = notation
                    f1.write("Classifies : " + res6["prefLabel"])
                    f1.write("\n")
                    f1.write("Notation : " + notation)
                    f1.write("\n")
        f1.write("\n")
'''

'''
print_properties(search_results)

f1.close()


writeOutput = open('classes_search_terms.txt', 'w')
for item in classes_terms:
    writeOutput.write("%s\n" % item)

writeOutput = open('classes_notation.txt', 'w')
for k, v in classes_dict.items():
    writeOutput.write("Class : " + str(k) + '\n' + "Notation : " + str(v) + '\n\n')

writeOutput = open('PT_notation.txt', 'w')
for k, v in classified_as_dict.items():
    writeOutput.write("Classified as : " + str(k) + '\n' + "Notation : " + str(v) + '\n\n')

writeOutput = open('PT_notation.txt', 'a')
for k, v in classifies_dict.items():
   writeOutput.write("Classifies : " + str(k) + '\n' + "Notation : " + str(v) + '\n\n')

'''

