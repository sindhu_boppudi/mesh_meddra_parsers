from auth.CAS.REST import service
from lxml.html import fromstring
from auth import *
import requests
import json
import argparse

uri = "https://utslogin.nlm.nih.gov"
auth_endpoint = "/cas/v1/api-key"

string = "Human insulin produced by recombinant DNA technology is the first commercial health care product derived from this technology. Work on this product was initiated before there were federal guidelines for large-scale recombinant DNA work or commercial development of recombinant DNA products. The steps taken to facilitate acceptance of large-scale work and proof of the identity and safety of such a product are described. While basic studies in recombinant DNA technology will continue to have a profound impact on research in the life sciences, commercial applications may well be controlled by economic conditions and the availability of investment capital."
#string = "elevated cholesterol"
#string = "BACKGROUND:Anti-tumor necrosis factor (anti-TNF) therapies are effective treatments for inflammatory bowel diseases (IBD). However, infections, psoriasis, and eczema are potential manifestations. Descriptions of these are limited. Our aim was to characterize these skin manifestations in children with IBD on anti-TNF therapy.METHODS:Our study is a retrospective review of IBD patients ranging in age from 6 to 18 years who were treated with anti-TNFs from 2010-2015. Data collected included demographics, clinical information, anti-TNF therapy used, and whether patients developed skin manifestations and their type of complication, clinical interventions, and outcomes.RESULTS:Of the 409 patients analyzed, 47 (11.4%) developed dermatologic manifestations (39 CD, 8 UC/IC). Among these 47 patients, there were 72 manifestations of infections (28/72; 38.9%), psoriasis (33/72; 45.8%), and eczema (10/72; 13.9%). There was no significant difference between patients with CD and UC/IC in the type of manifestation. Children on infliximab experienced an increased risk of psoriasis than those on adalimumab (P = 0.05). A greater percentage of female patients developed a skin manifestation (28/47; 60%). The majority of patients with a skin manifestation were able to continue the current anti-TNF regimen. Amongst the patients that developed psoriasis, 60% did not require change in anti-TNF therapy.CONCLUSIONS:This is the largest study analyzing anti-TNF related skin manifestations in a pediatric IBD cohort. Psoriasiform lesions were the most prevalent dermatological manifestation, and females experienced more reactions than males. Most patients were able to continue their anti-TNF therapy. However, if a change was required, it was most likely among those who developed psoriasis and required either a dose or interval change, different anti-TNF medication, or a medication class change"
#string = "high cholesterol"
sabs = "MSH"
pageNumber = 1
returnIdType = "code"
searchType = "exact"

class Authentication:
    def __init__(self, apikey):
        self.apikey = "7089be81-7237-4d83-8fa9-6c650a59440f"
        self.service = "http://umlsks.nlm.nih.gov"

    def gettgt(self):
        params = {'apikey': self.apikey}
        h = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain", "User-Agent": "python"}
        r = requests.post(uri + auth_endpoint, data=params, headers=h)
        response = fromstring(r.text)
        tgt = response.xpath('//form/@action')[0]
        return tgt

    def getst(self, tgt):
        params = {'service': self.service}
        h = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain", "User-Agent": "python"}
        r = requests.post(tgt, data=params, headers=h)
        st = r.text
        return st

    def getTerms(self, version, apikey):
        uri = "https://uts-ws.nlm.nih.gov"
        content_endpoint = "/rest/search/" + version
        AuthClient = Authentication(apikey)
        tgt = AuthClient.gettgt()
        ticket = AuthClient.getst(tgt)
        query = {'string': string, 'ticket': ticket, 'sabs': sabs}
        r = requests.get(uri + content_endpoint, params=query)
        r.encoding = 'utf-8'
        items = json.loads(r.text)
        jsonData = items["result"]

        print(items)


auth = Authentication(apikey='')
auth.getTerms(version='current', apikey='')

